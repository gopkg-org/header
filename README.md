# Header

This package provides the HTTP header constants available on [MDN Web Doc] in the Go language.

[MDN Web Doc]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
