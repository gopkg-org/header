package header

const (
	// Accept informs the server about the types of data that can be sent back.
	Accept = "Accept"

	// AcceptCH servers can advertise support for Client Hints using the Accept-CH header field or an equivalent
	// HTML <meta> element with http-equiv attribute.
	AcceptCH = "Accept-CH"

	// AcceptCHLifetime servers can ask the client to remember the set of Client Hints that the server supports for
	// a specified period of time, to enable delivery of Client Hints on subsequent requests to the server's origin.
	// Deprecated: This feature is no longer recommended.
	AcceptCHLifetime = "Accept-CH-Lifetime"

	// AcceptCharset request header was a header that advertised a client's supported character encodings.
	// It is no longer widely used.
	AcceptCharset = "Accept-Charset"

	// AcceptEncoding the encoding algorithm, usually a compression algorithm, that can be used on the resource
	// sent back.
	AcceptEncoding = "Accept-Encoding"

	// AcceptLanguage informs the server about the human language the server is expected to send back.
	// This is a hint and is not necessarily under the full control of the user: the server should always pay
	// attention not to override an explicit user choice (like selecting a language from a dropdown).
	AcceptLanguage = "Accept-Language"

	// AcceptPatch response header advertises which media-type the server is able to understand in a PATCH request.
	AcceptPatch = "Accept-Patch"

	// AcceptPost response header advertises which media types are accepted by the server for HTTP post requests.
	AcceptPost = "Accept-Post"

	// AcceptRanges indicates if the server supports range requests, and if so in which unit the range can be
	// expressed.
	AcceptRanges = "Accept-Ranges"

	// AccessControlAllowCredentials indicates whether the response to the request can be exposed when the credentials
	// flag is true.
	AccessControlAllowCredentials = "Access-Control-Allow-Credentials"

	// AccessControlAllowHeaders used in response to a preflight request to indicate which HTTP headers can be used
	// when making the actual request.
	AccessControlAllowHeaders = "Access-Control-Allow-Headers"

	// AccessControlAllowMethods specifies the methods allowed when accessing the resource in response to a preflight
	// request.
	AccessControlAllowMethods = "Access-Control-Allow-Methods"

	// AccessControlAllowOrigin indicates whether the response can be shared.
	AccessControlAllowOrigin = "Access-Control-Allow-Origin"

	// AccessControlExposeHeaders indicates which headers can be exposed as part of the response by listing their
	// names.
	AccessControlExposeHeaders = "Access-Control-Expose-Headers"

	// AccessControlMaxAge indicates how long the results of a preflight request can be cached.
	AccessControlMaxAge = "Access-Control-Max-Age"

	// AccessControlRequestHeaders used when issuing a preflight request to let the server know which HTTP headers
	// will be used when the actual request is made.
	AccessControlRequestHeaders = "Access-Control-Request-Headers"

	// AccessControlRequestMethod used when issuing a preflight request to let the server know which HTTP method will
	// be used when the actual request is made.
	AccessControlRequestMethod = "Access-Control-Request-Method"

	// Age is the time, in seconds, that the object has been in a proxy cache.
	Age = "Age"

	// Allow lists the set of HTTP request methods supported by a resource.
	Allow = "Allow"

	// AltSvc used to list alternate ways to reach this service.
	AltSvc = "Alt-Svc"

	// AltUsed used to identify the alternative service in use.
	AltUsed = "Alt-Used"

	// Authorization contains the credentials to authenticate a user-agent with a server.
	Authorization = "Authorization"

	// CacheControl directives for caching mechanisms in both requests and responses.
	CacheControl = "Cache-Control"

	// ClearSiteData clears browsing data (e.g. cookies, storage, cache) associated with the requesting website.
	ClearSiteData = "Clear-Site-Data"

	// Connection controls whether the network connection stays open after the current transaction finishes.
	Connection = "Connection"

	// ContentDisposition indicates if the resource transmitted should be displayed inline
	// (default behavior without the header), or if it should be handled like a download and the browser should
	// present a "Save As" dialog.
	ContentDisposition = "Content-Disposition"

	// ContentDPR response header used to confirm the image device to pixel ratio in requests where the DPR client
	// hint was used to select an image resource.
	// Deprecated: This feature is no longer recommended.
	ContentDPR = "Content-DPR"

	// ContentEncoding used to specify the compression algorithm.
	ContentEncoding = "Content-Encoding"

	// ContentLanguage describes the human language(s) intended for the audience, so that it allows a user to
	// differentiate according to the users' own preferred language.
	ContentLanguage = "Content-Language"

	// ContentLength the size of the resource, in decimal number of bytes.
	ContentLength = "Content-Length"

	// ContentLocation indicates an alternate location for the returned data.
	ContentLocation = "Content-Location"

	// ContentRange indicates where in a full body message a partial message belongs.
	ContentRange = "Content-Range"

	// ContentSecurityPolicy controls resources the user agent is allowed to load for a given page.
	ContentSecurityPolicy = "Content-Security-Policy"

	// ContentSecurityPolicyReportOnly allows web developers to experiment with policies by monitoring,
	// but not enforcing, their effects.
	// These violation reports consist of JSON documents sent via an HTTP POST request to the specified URI.
	ContentSecurityPolicyReportOnly = "Content-Security-Policy-Report-Only"

	// ContentType indicates the media type of the resource.
	ContentType = "Content-Type"

	// Cookie contains stored HTTP cookies previously sent by the server with the Set-Cookie header.
	Cookie = "Cookie"

	// CriticalCH servers use Critical-CH along with Accept-CH to specify that accepted client hints are also
	// critical client hints.
	CriticalCH = "Critical-CH"

	// CrossOriginEmbedderPolicy allows a server to declare an embedder policy for a given document.
	CrossOriginEmbedderPolicy = "Cross-Origin-Embedder-Policy"

	// CrossOriginOpenerPolicy prevents other domains from opening/controlling a window.
	CrossOriginOpenerPolicy = "Cross-Origin-Opener-Policy"

	// CrossOriginResourcePolicy prevents other domains from reading the response of the resources to which this header is applied.
	CrossOriginResourcePolicy = "Cross-Origin-Resource-Policy"

	// Date contains the date and time at which the message was originated.
	Date = "Date"

	// DeviceMemory approximate amount of available client RAM memory.
	// Deprecated: This feature is no longer recommended.
	DeviceMemory = "Device-Memory"

	// Digest
	// Deprecated: This feature is no longer recommended.
	Digest = "Digest"

	// DNT
	// Deprecated: This feature is no longer recommended.
	DNT = "DNT"

	// Downlink approximate bandwidth of the client's connection to the server, in Mbps.
	Downlink = "Downlink"

	// DPR client device pixel ratio (DPR), which is the number of physical device pixels corresponding to every
	// CSS pixel.
	// Deprecated: This feature is no longer recommended.
	DPR = "DPR"

	// EarlyData indicates that the request has been conveyed in TLS early data.
	EarlyData = "Early-Data"

	// ECT The effective connection type ("network profile") that best matches the connection's latency and bandwidth.
	ECT = "ECT"

	// ETag a unique string identifying the version of the resource.
	// Conditional requests using If-Match and If-None-Match use this value to change the behavior of the request.
	ETag = "ETag"

	// Expect indicates expectations that need to be fulfilled by the server to properly handle the request.
	Expect = "Expect"

	// ExpectCT allows sites to opt in to reporting and/or enforcement of Certificate Transparency requirements,
	// which prevents the use of misissued certificates for that site from going unnoticed.
	// When a site enables the Expect-CT header, they are requesting that Chrome check that any certificate for that
	// site appears in public CT logs.
	ExpectCT = "Expect-CT"

	// Expires the date/time after which the response is considered stale.
	Expires = "Expires"

	// Forwarded contains information from the client-facing side of proxy servers that is altered or lost when a
	// proxy is involved in the path of the request.
	Forwarded = "Forwarded"

	// From contains an Internet email address for a human user who controls the requesting user agent.
	From = "From"

	// Host specifies the domain name of the server (for virtual hosting), and (optionally) the TCP port number on
	// which the server is listening.
	Host = "Host"

	// IfMatch makes the request conditional, and applies the method only if the stored resource matches one of
	// the given ETags.
	IfMatch = "If-Match"

	// IfModifiedSince makes the request conditional, and expects the resource to be transmitted only if it has been
	// modified after the given date. This is used to transmit data only when the cache is out of date.
	IfModifiedSince = "If-Modified-Since"

	// IfNoneMatch makes the request conditional, and applies the method only if the stored resource doesn't match
	// any of the given ETags. This is used to update caches (for safe requests), or to prevent uploading a new
	// resource when one already exists.
	IfNoneMatch = "If-None-Match"

	// IfRange creates a conditional range request that is only fulfilled if the given etag or date matches the
	// remote resource. Used to prevent downloading two ranges from incompatible version of the resource.
	IfRange = "If-Range"

	// IfUnmodifiedSince makes the request conditional, and expects the resource to be transmitted only if it has not
	// been modified after the given date. This ensures the coherence of a new fragment of a specific range with
	// previous ones, or to implement an optimistic concurrency control system when modifying existing documents.
	IfUnmodifiedSince = "If-Unmodified-Since"

	// KeepAlive controls how long a persistent connection should stay open.
	KeepAlive = "Keep-Alive"

	// LargeAllocation tells the browser that the page being loaded is going to want to perform a large allocation.
	// Deprecated: This feature is no longer recommended.
	LargeAllocation = "Large-Allocation"

	// LastModified the last modification date of the resource, used to compare several versions of the same resource.
	// It is less accurate than ETag, but easier to calculate in some environments. Conditional requests using
	// If-Modified-Since and If-Unmodified-Since use this value to change the behavior of the request.
	LastModified = "Last-Modified"

	// Link entity-header field provides a means for serializing one or more links in HTTP headers.
	// It is semantically equivalent to the HTML <link> element.
	Link = "Link"

	// Location indicates the URL to redirect a page to.
	Location = "Location"

	// MaxForwards when using TRACE, indicates the maximum number of hops the request can do before being reflected
	// to the sender.
	MaxForwards = "Max-Forwards"

	// NEL defines a mechanism that enables developers to declare a network error reporting policy.
	NEL = "NEL"

	// Origin indicates where a fetch originates from.
	Origin = "Origin"

	// Pragma implementation-specific header that may have various effects anywhere along the request-response chain.
	// Used for backwards compatibility with HTTP/1.0 caches where the Cache-Control header is not yet present.
	// Deprecated: This feature is no longer recommended.
	Pragma = "Pragma"

	// PermissionsPolicy provides a mechanism to allow and deny the use of browser features in a website's own frame,
	// and in <iframe>s that it embeds.
	PermissionsPolicy = "Permissions-Policy"

	// ProxyAuthenticate defines the authentication method that should be used to access a resource behind a
	// proxy server.
	ProxyAuthenticate = "Proxy-Authenticate"

	// ProxyAuthorization contains the credentials to authenticate a user agent with a proxy server.
	ProxyAuthorization = "Proxy-Authorization"

	// Range indicates the part of a document that the server should return.
	Range = "Range"

	// Referer the address of the previous web page from which a link to the currently requested page was followed.
	Referer = "Referer"

	// ReferrerPolicy governs which referrer information sent in the Referer header should be included with
	// requests made.
	ReferrerPolicy = "Referrer-Policy"

	// RetryAfter indicates how long the user agent should wait before making a follow-up request.
	RetryAfter = "Retry-After"

	// RTT Application layer round trip time (RTT) in milliseconds, which includes the server processing time.
	RTT = "RTT"

	// SaveData a string on that indicates the user agent's preference for reduced data usage.
	SaveData = "Save-Data"

	// SecCHPrefersColorScheme user preference media feature client hint request header provides the user's
	// preference for light or dark color themes. A user indicates their preference through an operating system
	// setting (for example, light or dark mode) or a user agent setting.
	SecCHPrefersColorScheme = "Sec-CH-Prefers-Color-Scheme"

	// SecCHPrefersReducedMotion user agent client hint request header indicates the user agent's preference for
	// animations to be displayed with reduced motion.
	SecCHPrefersReducedMotion = "Sec-CH-Prefers-Reduced-Motion"

	// SecCHPrefersReducedTransparency user agent client hint request header indicates the user agent's preference
	// for reduced transparency.
	SecCHPrefersReducedTransparency = "Sec-CH-Prefers-Reduced-Transparency"

	// SecCHUA user agent's branding and version.
	SecCHUA = "Sec-CH-UA"

	// SecCHUAArch user agent's underlying platform architecture.
	SecCHUAArch = "Sec-CH-UA-Arch"

	// SecCHUABitness user agent's underlying CPU architecture bitness (for example "64" bit).
	SecCHUABitness = "Sec-CH-UA-Bitness"

	// SecCHUAFullVersion user agent's full semantic version string.
	// Deprecated: This feature is no longer recommended.
	SecCHUAFullVersion = "Sec-CH-UA-Full-Version"

	// SecCHUAFullVersionList full version for each brand in the user agent's brand list.
	SecCHUAFullVersionList = "Sec-CH-UA-Full-Version-List"

	// SecCHUAMobile user agent is running on a mobile device or, more generally, prefers a "mobile" user experience.
	SecCHUAMobile = "Sec-CH-UA-Mobile"

	// SecCHUAModel user agent's device model.
	SecCHUAModel = "Sec-CH-UA-Model"

	// SecCHUAPlatform user agent's underlying operation system/platform.
	SecCHUAPlatform = "Sec-CH-UA-Platform"

	// SecCHUAPlatformVersion user agent's underlying operation system version.
	SecCHUAPlatformVersion = "Sec-CH-UA-Platform-Version"

	// SecFetchDest indicates the request's destination. It is a Structured Header whose value is a token with
	// possible values audio, audioworklet, document, embed, empty, font, image, manifest, object, paintworklet,
	// report, script, serviceworker, sharedworker, style, track, video, worker, and xslt.
	SecFetchDest = "Sec-Fetch-Dest"

	// SecFetchMode indicates the request's mode to a server. It is a Structured Header whose value is a token with
	// possible values cors, navigate, no-cors, same-origin, and websocket.
	SecFetchMode = "Sec-Fetch-Mode"

	// SecFetchSite indicates the relationship between a request initiator's origin and its target's origin. It is
	// a Structured Header whose value is a token with possible values cross-site, same-origin, same-site, and none.
	SecFetchSite = "Sec-Fetch-Site"

	// SecFetchUser indicates whether or not a navigation request was triggered by user activation. It is a
	// Structured Header whose value is a boolean so possible values are ?0 for false and ?1 for true.
	SecFetchUser = "Sec-Fetch-User"

	// SecGPC indicates whether the user consents to a website or service selling or sharing their personal
	// information with third parties.
	SecGPC = "Sec-GPC"

	// SecPurpose indicates the purpose of the request, when the purpose is something other than immediate use by the
	// user-agent. The header currently has one possible value, prefetch, which indicates that the resource is being
	// fetched preemptively for a possible future navigation.
	SecPurpose = "Sec-Purpose"

	// SecWebSocketAccept ...
	SecWebSocketAccept = "Sec-WebSocket-Accept"

	// Server contains information about the software used by the origin server to handle the request.
	Server = "Server"

	// ServerTiming communicates one or more metrics and descriptions for the given request-response cycle.
	ServerTiming = "Server-Timing"

	// ServiceWorkerNavigationPreload a request header sent in preemptive request to fetch() a resource during service
	// worker boot. The value, which is set with NavigationPreloadManager.setHeaderValue(), can be used to inform a
	// server that a different resource should be returned than in a normal fetch() operation.
	ServiceWorkerNavigationPreload = "Service-Worker-Navigation-Preload"

	// SetCookie send cookies from the server to the user-agent.
	SetCookie = "Set-Cookie"

	// SourceMap links generated code to a source map.
	SourceMap = "SourceMap"

	// StrictTransportSecurity force communication using HTTPS instead of HTTP.
	StrictTransportSecurity = "Strict-Transport-Security"

	// TE specifies the transfer encodings the user agent is willing to accept.
	TE = "TE"

	// TimingAllowOrigin specifies origins that are allowed to see values of attributes retrieved via features of
	// the Resource Timing API, which would otherwise be reported as zero due to cross-origin restrictions.
	TimingAllowOrigin = "Timing-Allow-Origin"

	// TK indicates the tracking status that applied to the corresponding request.
	// Deprecated: This feature is no longer recommended.
	TK = "TK"

	// Trailer allows the sender to include additional fields at the end of chunked message.
	Trailer = "Trailer"

	// TransferEncoding specifies the form of encoding used to safely transfer the resource to the user.
	TransferEncoding = "Transfer-Encoding"

	// Upgrade the relevant RFC document for the Upgrade header field is RFC 9110, section 7.8.
	// The standard establishes rules for upgrading or changing to a different protocol on the current client,
	// server, transport protocol connection. For example, this header standard allows a client to change from
	// HTTP 1.1 to WebSocket, assuming the server decides to acknowledge and implement the Upgrade header field.
	// Neither party is required to accept the terms specified in the Upgrade header field. It can be used in both
	// client and server headers. If the Upgrade header field is specified, then the sender MUST also send the
	// Connection header field with the upgrade option specified. For details on the Connection header field please
	// see section 7.6.1 of the aforementioned RFC.
	Upgrade = "Upgrade"

	// UpgradeInsecureRequests sends a signal to the server expressing the client's preference for an encrypted and
	// authenticated response, and that it can successfully handle the upgrade-insecure-requests directive.
	UpgradeInsecureRequests = "Upgrade-Insecure-Requests"

	// UserAgent contains a characteristic string that allows the network protocol peers to identify the
	// application type, operating system, software vendor or software version of the requesting software user agent.
	UserAgent = "User-Agent"

	// Vary determines how to match request headers to decide whether a cached response can be used rather than
	// requesting a fresh one from the origin server.
	Vary = "Vary"

	// Via added by proxies, both forward and reverse proxies, and can appear in the request headers and the
	// response headers.
	Via = "Via"

	// ViewportWidth a number that indicates the layout viewport width in CSS pixels.
	// The provided pixel value is a number rounded to the smallest following integer (i.e. ceiling value).
	// Deprecated: This feature is no longer recommended.
	ViewportWidth = "Viewport-Width"

	// WantDigest is primarily used in a request, to ask the server to provide a digest of the requested resource
	// using the Digest response header.
	// Deprecated: This feature is no longer recommended.
	WantDigest = "Want-Digest"

	// Warning general information about possible problems.
	// Deprecated: This feature is no longer recommended.
	Warning = "Warning"

	// Width a number that indicates the desired resource width in physical pixels (i.e. intrinsic size of an image).
	// Deprecated: This feature is no longer recommended.
	Width = "Width"

	// WWWAuthenticate defines the authentication method that should be used to access a resource.
	WWWAuthenticate = "WWW-Authenticate"

	// XContentTypeOptions disables MIME sniffing and forces browser to use the type given in Content-Type.
	XContentTypeOptions = "X-Content-Type-Options"

	// XDNSPrefetchControl controls DNS prefetching, a feature by which browsers proactively perform domain name
	// resolution on both links that the user may choose to follow as well as URLs for items referenced by the
	// document, including images, CSS, JavaScript, and so forth.
	XDNSPrefetchControl = "X-DNS-Prefetch-Control"

	// XForwardedFor identifies the originating IP addresses of a client connecting to a web server through an
	// HTTP proxy or a load balancer.
	XForwardedFor = "X-Forwarded-For"

	// XForwardedHost identifies the original host requested that a client used to connect to your proxy or
	// load balancer.
	XForwardedHost = "X-Forwarded-Host"

	// XForwardedPort identifies the original port requested that a client used to connect to your proxy or
	// load balancer.
	XForwardedPort = "X-Forwarded-Port"

	// XForwardedProto identifies the original protocol requested that a client used to connect to your proxy or
	// load balancer.
	XForwardedProto = "X-Forwarded-Proto"

	// XFrameOptions indicates whether a browser should be allowed to render a page in a <frame>, <iframe>, <embed>
	// or <object>.
	XFrameOptions = "X-Frame-Options"

	// XRealIP identifies the originating IP addresses of a client connecting to a web server through an
	// HTTP proxy or a load balancer.
	XRealIP = "X-Real-Ip"

	// XRemoteIp identifies the client's IP address.
	XRemoteIp = "X-Remote-Ip"

	// XRemotePort identifies the client's port.
	XRemotePort = "X-Remote-Port"

	// XRemoteProto identifies the client's protocol.
	XRemoteProto = "X-Remote-Proto"

	// XRequestID correlates HTTP requests between a client and server.
	XRequestID = "X-Request-Id"

	// XXSSProtection enables cross-site scripting filtering.
	XXSSProtection = "X-XSS-Protection"

	// XCSRFToken Cross-Site Request Forgery identifier
	XCSRFToken = "X-Csrf-Token"
)
